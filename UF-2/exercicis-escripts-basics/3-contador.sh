#! /bin/bash
# @edt ASIX M01-ISo Curs 2022-2023
#   Contador de zero a MAX
#
# -------------------------------------------------------------------
num=0
MAX="$1"
while [ $num -le $MAX ]
do	
  echo "$num"
  num=$((num+1))  
done
exit 0
