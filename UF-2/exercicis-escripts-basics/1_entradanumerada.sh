#! /bin/bash
# @edt ASIX M01-ASIX 2022-2023
#
# Descripcio: mostrar l'entrada numerada
# -------------------------------------------------------------------
num=1
while read -r line
do	
  echo "$num: $line"
  num=$((num+1))  
done
exit 0
