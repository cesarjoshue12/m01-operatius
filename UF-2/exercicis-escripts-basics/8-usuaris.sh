#! /bin/bash

for usuari in $*
do
 grep "^$usuari:" /etc/passwd &>/dev/null
 if [ $? -eq 0 ];
   then
     echo "$usuari: si existeix"
  else
    echo "$usuari: no existeix" >> error.log
 fi
done
exit 0
