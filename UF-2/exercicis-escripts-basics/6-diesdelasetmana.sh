#! /bin/bash
# @edt ASIX-M01 Curs 2022-2023
# Març 2023
# Descripcio: programa que rep dies de la setmana i mostra els dies laborables i festius 
# ------------------------

#0) validar arguments
ERR_NARG=1
if [ $# -eq 0 ]
then
  echo "ERROR: numero argument incorrecte"
  echo "USAGE: $# dia de la setmana"
  exit $ERR_NARG
fi

#2 ) xixa
laborables=0
festiu=0
for dia in $*
do
  case $dia in
   "dilluns"|"dimarts"|"dimecres"|"dijous"|"divendres")
     ((laborables++))
     ;;
   "dissabte"|"diumenge")
     ((festiu++))
     ;;
   *)
     echo "$dia: No és un dia de la setmana" >> error.log
  esac
done

echo "laborables: $laborables i festius: $festiu"
exit 0
