#! /bin/bash
# @edt ASIX-M01
# Febrer 2023
#
# Validar dos arguments
# i mostrar

# -----------------------------------------------
# si num args no és correcte
if [ $# -ne 2 ]
then
  echo "ERROR: num args incorrecte"
  echo "USAGE: $0 nom edat"
  exit 1
fi

# 2) xixa
echo "nom: $1"
echo "edat: $2"
exit 0
