#! /bin/bash
# @edt ASIX-M01
# Febrer 2023
#
# Sinopsis: prog dir
# Validar que es rep un argument i que és un directori i llistar-ne el contingut
#Per llistar el contingut a,b un simple ls
#
# -----------------------------------------------------------------------------
#1) Validar arguments
ERR_NARG=1
ERR_NODIR=2

if [ $# -ne 1 ]
then
  echo "ERROR: numero argumetns incorrecte"
  echo "USAGE: $0 directori"
  exit $ERR_NARG
fi

dir=$1

#2) Validar és un argument 
if [ ! -d $dir ]
then
  echo "ERROR: no és un directori"
  echo "USAGE: $0 ha de ser un directori"
  exit $ERR_NODIR
fi

#3) XIXA
ls $dir

