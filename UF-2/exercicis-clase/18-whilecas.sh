#! /bin/bash
# MARÇ 2023
# @edt ASIX M01

#------------------------------------
opcions=""
arguments=""
myfile=""
num=""


while [ -n "$1" ]
do
  case $1 in
   "-a"|"-b"|"-c"|"-d")
   opcions="$opcions $1" ;;
   "-f" )
     myfile="$myfile $2"
     shift ;;
   "-n")
     num="$num $2"
     shift;;
   *)
     arguments="$arguments $1" ;;
  esac
  shift
done

echo "opcions: $opcions"
echo "arguments: $arguments"
echo "file: $myfile"
echo "num: $num"
