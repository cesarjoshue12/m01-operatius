#! /bin/bash
# @edt ASIX-M01
# Febrer 2023
#
# Validar si te un argument
# Validar si la nota és valida
# $ prog validar nota

# --------------------------------


# 1) validar si existeix un argument

if [ $# -ne 1 ]
then
  echo "ERROR: numero argument incorrecte"
  echo "USAGE: $0 nota"
  exit 1
fi

if [ $1 -gt 10 ]
then
  echo "ERROR: nota $1 no valida"
  echo "nota pren valors de 0 a 10"
  echo "USAGE $0 nota"
  exit 2
fi

# 2) xixa

nota=$1
if [ $nota -lt 5 ]
then
  echo "Suspès"
else
  echo "Aprovat"
fi

exit 0
