#! /bin/bash
# MARÇ 2023
# @edt ASIX M01

# 17-argfor.sh [ -a -b -c -d ] arg
#------------------------------------

# Crear una lista vacio i ir añadiendo argumentos
opcions=""
arguments=""

for arg in $*
do
  case $arg in 
  "-a"|"-b"|"-c"|"-d")
    opcions="$opcions $arg";;
  *)
    arguments="$arguments $arg";; 
  esac
done

echo "opcions: $opcions"
echo "arguments: $arguments"
