#! /bin/bash
# @edt ASIX-M01
# MARÇ 2023
#
# Sinopsis: prog dir
# Validar que es rep un argument i que és un directori i llistar-ne el contingut
#Per llistar el contingut a,b un simple ls
#
# -----------------------------------------------------------------------------
#1) Validar arguments
ERR_NARG=1
ERR_NODIR=2

if [ $# -eq 0 ]
then
  echo "ERROR: numero argumetns incorrecte"
  echo "USAGE: $0 directori"
  exit $ERR_NARG
fi

dir=$1

#2) Validar que cada argument és un diretori

for dir in $*
do 
  if [ ! -d $dir ]; then
    echo "EROR. $dir no es un dircotri" 1>&2    # Els error de stdout envials a stderrº
  else  					# si és un directori l'afegim a una llista
    llista=$(ls $dir)
    echo "dir: $dir"
    for elem in $llista 			#Per cada directori de la llista diem que es cada cosa
    do
      if [ -h $dir/$elem ]; then
        echo -e "\t$elem: és un link" 
      elif [ -f $dir/$elem ]; then
        echo -e "\t$elem: és un reguar file"
      elif [ -d $dir/$elem ]; then
        echo -e "\t$elem: és un directori"
      else
        echo -e "\t$elem: és altre"
      fi
    done
  fi
done
exit 0
