#! /bin/bash
# @edt ASIX-M01
# Febrer 2023
#
# Programa que validar si l'argument és un directori
# ------------------------

# 1) Validar num argument
if [ $# -ne 1 ]
then
 echo "ERROR: nums arguments incorrecte"
 echo "USAGE: $0 dir"
 exit 1
fi

# 2) Validar que sigui directori
if [ ! -d $1 ]	
then
 echo "ERROR: $1 No és directori"
 echo "USAGE: $0 director"
 exit 2
fi

#xixa
dir=$1
ls $dir
exit 0




