#! /bin/bash
# @edt ASIX-M01
# Febrer 2023
#
#Exemples case
# ----------

case $1 in
    "ds"|"dm")
     #d[sm]
     echo "$1 és festiu"
     ;;
    "dl"|"dt"|"dc"|"dj"|"dv")
    # d[ltcjv]
     echo "$1 és laborable"
     ;;
    *)    
     echo "($1) no es un dia"
     ;;
esac
exit 0

case $1 in
  [aeiou])
     echo "és una vocal"
     ;;
  [bcdfghjklmnpqrstvwxyz])
     echo "és una consonant"
     ;;
  *)
     echo "és una altra cosa"
     ;;

esac
exit 0


case $1 in
   "pere"|"pau"|"joan")
      echo "és un nen"
      ;;
    "marta"|"anna"|"julia")
      echo "és una nena"
      ;;
   *)
    echo "no binari"
    ;;

esac
exit 0
