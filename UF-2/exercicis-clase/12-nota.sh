#! /bin/bash
# @edt ASIX-M01
# Febrer 2023
#
# Programa que validar si l'argument és un directori, un regular file o un simbolic link 
# ------------------------

#Validar arguments
if [ ! $# -eq 1 ]
then
  echo "ERROR : num arguments incorrecte"
  echo "USAGE: $0 nota"
  exit 1

#Xixa

for nota in $*
do
  if ! [ $nota -ge 0 -a $nota -le 10 ] ;then
    echo "ERROR: Instrodueix una nota correcte (0-10)" >> /dev/stderr
  elif [ $nota -lt 5 ] ;then
    echo "$num: Has tret in $nota suspès."
  elif [ $nota -lt 7 ] ;then
    echo "Has aprovat "
  elif [ $nota -lt 9 ] ;then
    echo "$num: Has tret $num un notable"
  else
    echo "$num: Has tret un excel:lent"
  fi
done
