#! /bin/bash
# @edt ASIX-M01
# MARÇ  2023
#
#Programa et diu els dies que té el mes
# ----------

#1) Si nums arguments no és correcte plega
ERR_ARGUMENTS=1

if [ $# -ne 1 ]
then
 echo "ERROR: numer d'arguments no correcte"
 echo "USAGE: $0 mes"
 exit $ERR_ARGUMENTS

#2) Validar que es un mes(1-12)


#XIXA
case $1 in
 [13578]|"10"|"12")
  echo "té 31 dies"
  ;;
 [2469]|"11")
   echo "té 30 dies"
   ;;
  *)
   echo "No correspon"
esac
exit 0
