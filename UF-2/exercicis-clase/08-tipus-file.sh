#! /bin/bash
# @edt ASIX-M01
# Febrer 2023
#
# Programa que validar si l'argument és un directori, un regular file o un simbolic link 
# ------------------------


# 1) Si nums args no és correcte plega 
ERR_NARGS=1

if [ $# -ne 1 ]
then
  echo "ERROR: número d'arguments invàlids"
  echo "USAGE: $0 dir/link/file"
  exit $ERR_NARGS
fi

# 2) VALIDACIÓ ARGUMENT

arg=$1
if [ -d $arg ]
then
  echo "$arg és un directori"
elif [ -f $arg ]
then
  echo "$arg és un regular file"
elif [ -L $arg ]
then
  echo "$arg és un link"
elif ! [ -e $arg ]
then
  echo "$arg no existeix"
fi
exit 0 

